﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika
{
    using System;

    /// <summary>
    ///     Represents a token
    /// </summary>
    public class Token
    {
        /// <summary>
        /// Gets the location of the token
        /// </summary>
        public SourceLocation Location { get; private set; }

        /// <summary>
        ///     Initializes a token
        /// </summary>
        /// <param name="kind">The kind of token</param>
        /// <param name="value">The value of the token</param>
        public Token(TokenKind kind, string value = null)
            : this(kind, value, SourcePosition.Default, SourcePosition.Default)
        {
            Kind = kind;
            Value = value;
        }

        /// <summary>
        ///     Initializes a token
        /// </summary>
        /// <param name="kind">The kind of token</param>
        /// <param name="value">The value of the token</param>
        /// <param name="startPosition">The start position of the token</param>
        /// <param name="endPosition">The end position of the token</param>
        public Token(TokenKind kind, string value, SourcePosition startPosition, SourcePosition endPosition)
        {
            Kind = kind;
            Value = value;
            Location = new SourceLocation(startPosition, endPosition);
        }

        /// <summary>
        ///     Initializes a token
        /// </summary>
        /// <param name="kind">The kind of token</param>
        /// <param name="value">The value of the token</param>
        /// <param name="startPosition">The start position of the token</param>
        /// <param name="endPosition">The end position of the token</param>
        public Token(TokenKind kind, char value, SourcePosition startPosition, SourcePosition endPosition)
        {
            Kind = kind;
            Value = string.Format("{0}",value);
            Location = new SourceLocation(startPosition, endPosition);
        }

        /// <summary>
        ///     Gets the kind of token
        /// </summary>
        public TokenKind Kind { get; private set; }

        /// <summary>
        ///     Gets the value of the token
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override string ToString()
        {
            return string.Format("Kind: {0}, Value: {1}, Location: {2}", Kind, Value, Location);
        }
    }
}
