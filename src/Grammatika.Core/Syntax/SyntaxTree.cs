﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika.Syntax
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    /// <summary>
    ///     Represents the abstract syntax tree
    /// </summary>
    public class SyntaxTree
    {
        readonly List<SyntaxNode> _children;

        /// <summary>
        /// Creates a new SyntaxTree
        /// </summary>
        public SyntaxTree(params SyntaxNode[] children)
        {
            _children = new List<SyntaxNode>(children.Where(n => n != null));
        }

        /// <summary>
        ///     Parses the text into a syntax tree
        /// </summary>
        /// <param name="source">The source to parse</param>
        /// <returns>An instance of <see cref="SyntaxTree" /></returns>
        public static ParseResults ParseText(string source)
        {
            CharacterStream characterStream = new CharacterStream(source);
            Scanner scanner = new Scanner(characterStream);
            TokenStream tokenStream = new TokenStream(scanner);
            Parser parser = new Parser(tokenStream);
            return parser.ParseCompilationUnit();
        }

        /// <summary>
        /// Parses a file 
        /// </summary>
        /// <param name="path">The path</param>
        /// <returns>An instance of <see cref="SyntaxTree"/></returns>
        public static ParseResults Parse(string path)
        {
            string source;
            using (var stream = File.OpenRead(path))
            {
                using (var reader = new StreamReader(stream))
                {
                    source = reader.ReadToEnd();
                }
            }
            return ParseText(source);
        }

        /// <summary>
        /// Accepts a visitor
        /// </summary>
        /// <param name="visitor">A visitor</param>
        public void Accept(SyntaxTreeVisitor visitor)
        {
            foreach (var node in _children)
            {
                node.Accept(visitor);
            }
        }
    }
}
