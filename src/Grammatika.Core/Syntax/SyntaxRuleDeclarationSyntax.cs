﻿using System;

namespace Grammatika.Syntax
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents a syntax rule declaration
    /// </summary>
    public class SyntaxRuleDeclarationSyntax : RuleDeclarationSyntax
    {
        /// <summary>
        /// Create a new instance of SyntaxRuleDeclarationSyntax
        /// </summary>
        /// <param name="name"></param>
        /// <param name="expressions"></param>
        public SyntaxRuleDeclarationSyntax(string name, IEnumerable<RuleExpressionSyntax> expressions) : base(name, expressions)
        {
        }

        /// <summary>
        ///     Accepts a visitor
        /// </summary>
        /// <param name="visitor">The visitor to accept</param>
        public override void Accept(SyntaxTreeVisitor visitor)
        {
            visitor.OnStartSyntaxRuleDeclaration(this);
            base.Accept(visitor);
            visitor.OnEndSyntaxRuleDeclaration(this);
        }
    }
}