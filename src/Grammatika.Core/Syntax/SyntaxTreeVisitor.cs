﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika.Syntax
{
    using System;

    /// <summary>
    ///     Represents a syntax tree visitor
    /// </summary>
    public abstract class SyntaxTreeVisitor
    {
        /// <summary>
        ///     OnStartLanguageDefinition
        /// </summary>
        /// <param name="node">The language</param>
        public virtual void OnStartLanguageDefinition(LanguageDeclarationSyntax node)
        {
        }

        /// <summary>
        ///     OnEndLanguageDefinition
        /// </summary>
        /// <param name="node">The language</param>
        public virtual void OnEndLanguageDefinition(LanguageDeclarationSyntax node)
        {
        }

        /// <summary>
        ///     OnStringLiteral
        /// </summary>
        /// <param name="node"></param>
        public virtual void OnStringLiteral(StringLiteralRuleExressionSyntax node)
        {
        }

        /// <summary>
        ///     OnRuleReferenceExpression
        /// </summary>
        /// <param name="node"></param>
        public virtual void OnRuleReferenceExpression(RuleReferenceExpressionSyntax node)
        {
        }

        /// <summary>
        ///     OnStartSyntaxRuleDeclaration
        /// </summary>
        /// <param name="node"></param>
        public virtual void OnStartSyntaxRuleDeclaration(SyntaxRuleDeclarationSyntax node)
        {
        }

        /// <summary>
        ///     OnEndSyntaxRuleDeclaration
        /// </summary>
        /// <param name="node"></param>
        public virtual void OnEndSyntaxRuleDeclaration(SyntaxRuleDeclarationSyntax node)
        {
        }

        /// <summary>
        ///     OnStartTokenRuleDeclaration
        /// </summary>
        /// <param name="node"></param>
        public virtual void OnStartTokenRuleDeclaration(TokenRuleDeclarationSyntax node)
        {
        }

        /// <summary>
        ///     OnEndTokenRuleDeclaration
        /// </summary>
        /// <param name="node"></param>
        public virtual void OnEndTokenRuleDeclaration(TokenRuleDeclarationSyntax node)
        {
        }
    }
}
