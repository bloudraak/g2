﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika.Syntax
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Represents a token rule declaration
    /// </summary>
    public class TokenRuleDeclarationSyntax : RuleDeclarationSyntax
    {
        /// <summary>
        ///     Create a new instance of a token rule declaration
        /// </summary>
        /// <param name="name"></param>
        /// <param name="expressions"></param>
        public TokenRuleDeclarationSyntax(string name, IEnumerable<RuleExpressionSyntax> expressions)
            : base(name, expressions)
        {
        }

        /// <summary>
        ///     Accepts a visitor
        /// </summary>
        /// <param name="visitor">The visitor to accept</param>
        public override void Accept(SyntaxTreeVisitor visitor)
        {
            visitor.OnStartTokenRuleDeclaration(this);
            base.Accept(visitor);
            visitor.OnEndTokenRuleDeclaration(this);
        }
    }
}
