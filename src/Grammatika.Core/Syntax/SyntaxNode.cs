using System;

namespace Grammatika.Syntax
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents an syntax node
    /// </summary>
    public abstract class SyntaxNode
    {
        private readonly List<SyntaxNode> _children;

        /// <summary>
        /// Initializes a syntax node 
        /// </summary>
        /// <param name="children">Children of this node if any</param>
        protected SyntaxNode(params SyntaxNode[] children)
        {
            _children = new List<SyntaxNode>(children.Where(n => n != null));
        }

        /// <summary>
        /// Initializes a syntax node 
        /// </summary>
        /// <param name="children">Children of this node if any</param>
        protected SyntaxNode(IEnumerable<SyntaxNode> children)
        {
            _children = new List<SyntaxNode>(children.Where(n => n != null));
        }

        /// <summary>
        /// Returns the child nodes
        /// </summary>
        protected IEnumerable<SyntaxNode> Children
        {
            get { return _children; }
        }

        /// <summary>
        /// Accepts a visitor
        /// </summary>
        /// <param name="visitor">The visitor to accept</param>
        public virtual void Accept(SyntaxTreeVisitor visitor)
        {
            foreach (var node in Children)
            {
                node.Accept(visitor);
            }
        }
    }
}