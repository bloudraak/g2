﻿using System;

namespace Grammatika
{
    using System.IO;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a parer exception
    /// </summary>
    [Serializable]
    public class ParserException : Exception
    {
        private readonly ParserErrorCollection _errors;

        /// <summary>
        /// Initializes an instance of <see cref="ParserException"/>
        /// </summary>
        public ParserException()
        {
            _errors = new ParserErrorCollection();
        }

        /// <summary>
        /// Initializes an instance of <see cref="ParserException"/>
        /// </summary>
        public ParserException(string message) : base(message)
        {
            _errors = new ParserErrorCollection();
        }

        /// <summary>
        /// Initializes an instance of <see cref="ParserException"/>
        /// </summary>
        public ParserException(string message, Exception inner) : base(message, inner)
        {
            _errors = new ParserErrorCollection();
        }

        /// <summary>
        /// Initializes an instance of <see cref="ParserException"/>
        /// </summary>
        protected ParserException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
            _errors = new ParserErrorCollection();
        }

        /// <summary>
        /// Initializes an instance of <see cref="ParserException"/>
        /// </summary>
        public ParserException(ParserErrorCollection errors) :base(MakeMessage(errors))
        {
            _errors = new ParserErrorCollection(errors);
        }

        private static string MakeMessage(ParserErrorCollection errors)
        {
            if (errors == null)
            {
                throw new ArgumentNullException("errors");
            }
            using (var writer = new StringWriter())
            {
                foreach (var error in errors)
                {
                    writer.WriteLine(error);
                }
                return writer.ToString();
            }
        }
    }
}