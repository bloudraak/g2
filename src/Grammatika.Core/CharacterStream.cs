﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika
{
    using System;

    /// <summary>
    ///     Represents a character stream
    /// </summary>
    public class CharacterStream
    {
        /// <summary>
        ///     An invalid character
        /// </summary>
        public const char InvalidCharacter = char.MaxValue;

        private readonly int _length;
        private readonly string _source;

        /// <summary>
        ///     Initialises a new character stream from a <see cref="string" />
        /// </summary>
        /// <param name="source">The source to create the character string from</param>
        public CharacterStream(string source)
        {
            _source = source;
            _length = _source.Length;
            ColumnNumber = 1;
            LineNumber = 1;
        }

        /// <summary>
        /// Gets the current column number
        /// </summary>
        public int ColumnNumber { get; set; }

        /// <summary>
        /// Gets the current row number
        /// </summary>
        public int LineNumber { get; private set; }

        /// <summary>
        ///     The current position in the stream
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        ///     Returns the next character in the stream without advancing the position
        /// </summary>
        /// <returns>The next character or <see cref="InvalidCharacter" /> when there are no characters left</returns>
        public char Peek()
        {
            return _length <= Position ? InvalidCharacter : _source[Position];
        }

        /// <summary>
        ///     Returns the next character in the stream and advanced the position
        /// </summary>
        /// <returns>The next character or <see cref="InvalidCharacter" /> when there are no characters left</returns>
        public char Read()
        {
            if (_length <= Position)
            {
                return InvalidCharacter;
            }
            
            var c = _source[Position++];

            switch (c)
            {
                case '\n':
                    ColumnNumber = 1;
                    LineNumber++;
                    break;
                
                case '\t':
                    ColumnNumber += 4;
                    break;

                default:
                    ColumnNumber++;
                    break;
            }
            
            return c;
        }

        /// <summary>
        ///     Returns the next character in the stream without advancing the position
        /// </summary>
        /// <returns>The next character or <see cref="InvalidCharacter" /> when there are no characters left</returns>
        public char Peek(int k)
        {
            return _length <= Position + k ? InvalidCharacter : _source[Position + k];
        }
    }
}
