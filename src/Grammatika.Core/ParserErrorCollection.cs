// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    ///     Represents a parser error collection
    /// </summary>
    public class ParserErrorCollection : List<ParserError>
    {
        /// <summary>
        /// Represents an empty collection
        /// </summary>
        public static readonly ParserError[] Empty = new ParserError[0];


        /// <devdoc>
        ///     Initializes a new instance of <see cref='ParserErrorCollection' />.
        /// </devdoc>
        public ParserErrorCollection()
        {
        }

        /// <devdoc>
        ///     Initializes a new instance of <see cref='ParserErrorCollection' />.
        /// </devdoc>
        public ParserErrorCollection(int capacity) : base(capacity)
        {
        }

        /// <devdoc>
        ///     Initializes a new instance of <see cref='ParserErrorCollection' />.
        /// </devdoc>
        public ParserErrorCollection(IEnumerable<ParserError> collection) : base(collection)
        {
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override string ToString()
        {
            StringWriter writer = new StringWriter();
            foreach (var error in this)
            {
                writer.WriteLine(error);
            }
            return writer.ToString();
        }
    }
}
