﻿using System;

namespace Grammatika
{
    /// <summary>
    /// Represents an error code
    /// </summary>
    public enum ErrorCode
    {
        /// <summary>
        /// Reports a missing keyword
        /// </summary>
        MissingKeyword
    }
}