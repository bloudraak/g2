﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    ///     Represents a scanner
    /// </summary>
    public class Scanner
    {
        private readonly CharacterStream _stream;

        /// <summary>
        ///     Initialises a new scanner from an character stream
        /// </summary>
        /// <param name="stream">An instance of <see cref="CharacterStream" /></param>
        public Scanner(CharacterStream stream)
        {
            _stream = stream;
        }

        /// <summary>
        ///     Scans the tokens
        /// </summary>
        /// <returns>An enumeration of tokens</returns>
        public IEnumerable<Token> Scan()
        {
            char c = _stream.Peek();
            while (c != CharacterStream.InvalidCharacter)
            {
                SourcePosition endPosition = new SourcePosition(0,0);
                SourcePosition startPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber);
                switch (c)
                {

                    case ' ':
                    case '\r':
                    case '\n':
                    case '\t':
                        _stream.Read();
                        break;

                    case ';':
                        _stream.Read();
                        endPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber);
                        yield return new Token(TokenKind.SemicolonToken, ";", startPosition, endPosition);
                        break;

                    case '*':
                        _stream.Read();
                        endPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber);
                        yield return new Token(TokenKind.StarToken, "*", startPosition, endPosition);
                        break;

                    case '=':
                        _stream.Read();
                        endPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber);
                        yield return new Token(TokenKind.EqualsToken, "=", startPosition, endPosition);
                        break;

                    case '{':
                        _stream.Read();
                        endPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber);
                        yield return new Token(TokenKind.OpenBraceToken, "{", startPosition, endPosition);
                        break;

                    case '}':
                        _stream.Read();
                        endPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber);
                        yield return new Token(TokenKind.CloseBraceToken, "}", startPosition, endPosition);
                        break;

                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                    case 'f':
                    case 'g':
                    case 'h':
                    case 'i':
                    case 'j':
                    case 'k':
                    case 'l':
                    case 'm':
                    case 'n':
                    case 'o':
                    case 'p':
                    case 'q':
                    case 'r':
                    case 's':
                    case 't':
                    case 'u':
                    case 'v':
                    case 'w':
                    case 'x':
                    case 'y':
                    case 'z':
                    case 'A':
                    case 'B':
                    case 'C':
                    case 'D':
                    case 'E':
                    case 'F':
                    case 'G':
                    case 'H':
                    case 'I':
                    case 'J':
                    case 'K':
                    case 'L':
                    case 'M':
                    case 'N':
                    case 'O':
                    case 'P':
                    case 'Q':
                    case 'R':
                    case 'S':
                    case 'T':
                    case 'U':
                    case 'V':
                    case 'W':
                    case 'X':
                    case 'Y':
                    case 'Z':
                    case '_':
                        yield return IdentifierOrKeyword();
                        break;

                    case '\'':
                        yield return StringLiteral();
                        break;

                    case '/':
                        if (_stream.Peek(1) == '/')
                        {
                            do
                            {
                                _stream.Read();
                                c = _stream.Peek();
                            } while (c != '\n' && c != CharacterStream.InvalidCharacter);
                            _stream.Read();
                        }
                        break;

                    default:
                         _stream.Read();
                        endPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber);
                        yield return new Token(TokenKind.BadToken, c, startPosition, endPosition);
                        break;
                }
                
                c = _stream.Peek();
            }
        }

        private Token StringLiteral()
        {
            bool done = false;
            StringBuilder builder = new StringBuilder();
            
            SourcePosition startPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber);
            if (_stream.Peek() == '\'')
            {
                _stream.Read();
            }
            
            while (!done)
            {
                char c = _stream.Peek();
                switch (c)
                {
                    case '\'':
                        _stream.Read();
                        done = true;
                        break;

                    default:
                        builder.Append(_stream.Read());
                        break;
                }
            }
            var value = builder.ToString();
            SourcePosition endPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber); 
            return new Token(TokenKind.StringLiteralToken, value, startPosition, endPosition);
        }

        /// <summary>
        ///     Scans the character stream for an indentifier or keyword
        /// </summary>
        /// <returns></returns>
        private Token IdentifierOrKeyword()
        {
            bool done = false;
            SourcePosition startPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber);
            StringBuilder builder = new StringBuilder();
            while (!done)
            {
                char c = _stream.Peek();
                switch (c)
                {
                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                    case 'f':
                    case 'g':
                    case 'h':
                    case 'i':
                    case 'j':
                    case 'k':
                    case 'l':
                    case 'm':
                    case 'n':
                    case 'o':
                    case 'p':
                    case 'q':
                    case 'r':
                    case 's':
                    case 't':
                    case 'u':
                    case 'v':
                    case 'w':
                    case 'x':
                    case 'y':
                    case 'z':
                    case 'A':
                    case 'B':
                    case 'C':
                    case 'D':
                    case 'E':
                    case 'F':
                    case 'G':
                    case 'H':
                    case 'I':
                    case 'J':
                    case 'K':
                    case 'L':
                    case 'M':
                    case 'N':
                    case 'O':
                    case 'P':
                    case 'Q':
                    case 'R':
                    case 'S':
                    case 'T':
                    case 'U':
                    case 'V':
                    case 'W':
                    case 'X':
                    case 'Y':
                    case 'Z':
                    case '_':
                        builder.Append(_stream.Read());
                        break;

                    default:
                        done = true;
                        break;
                }
            }

            TokenKind kind;
            var value = builder.ToString();
            switch (value)
            {
                case "language":
                    kind = TokenKind.LanguageKeyword;
                    break;

                case "syntax":
                    kind = TokenKind.SyntaxKeyword;
                    break;

                case "token":
                    kind = TokenKind.SyntaxKeyword;
                    break;

                default:
                    kind = TokenKind.Identifier;
                    break;
            }
            SourcePosition endPosition = new SourcePosition(_stream.LineNumber, _stream.ColumnNumber); 
            return new Token(kind, value, startPosition, endPosition);
        }
    }
}
