﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika
{
    using System;
    using System.Collections.Generic;
    using Grammatika.Syntax;

    /// <summary>
    ///     Represents a parser
    /// </summary>
    public class Parser
    {
        private readonly TokenStream _stream;
        private readonly ParserErrorBuilder _errorBuilder;

        /// <summary>
        ///     Creates a parser from a token stream
        /// </summary>
        /// <param name="stream">The stream of tokens</param>
        public Parser(TokenStream stream)
        {
            _stream = stream;
            _errorBuilder = new ParserErrorBuilder();
        }

        /// <summary>
        ///     Parses a compilation unit
        /// </summary>
        /// <returns>an instance of <see cref="SyntaxTree" /></returns>
        public ParseResults ParseCompilationUnit()
        {
            LanguageDeclarationSyntax node;
            ParserErrorCollection errors = new ParserErrorCollection();
            ParseLanguageDeclaration(out node, errors);
            return new ParseResults(new SyntaxTree(node), errors);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="errors"></param>
        private void ParseLanguageDeclaration(out LanguageDeclarationSyntax node, ParserErrorCollection errors)
        {
            string name;
            ICollection<RuleDeclarationSyntax> nodes = new List<RuleDeclarationSyntax>();
 
            ConsumeToken(errors, TokenKind.LanguageKeyword);
            ConsumeToken(errors, TokenKind.Identifier, out name);
            ConsumeToken(errors, TokenKind.OpenBraceToken);
            ParseRuleDeclarations(nodes, errors);
            ConsumeToken(errors, TokenKind.CloseBraceToken);

            node = new LanguageDeclarationSyntax(name, nodes);
        }

        private void ParseRuleDeclarations(ICollection<RuleDeclarationSyntax> nodes, ParserErrorCollection errors)
        {
            while (_stream.Current.Kind == TokenKind.SyntaxKeyword || _stream.Current.Kind == TokenKind.TokenKeyword)
            {
                RuleDeclarationSyntax node;
                ParseRuleDeclaration(errors, out node);

                if (node != null)
                {
                    nodes.Add(node);
                }
            }
        }

        private void ParseRuleDeclaration(ParserErrorCollection errors, out RuleDeclarationSyntax node)
        {
            switch (_stream.Current.Kind)
            {
                case TokenKind.SyntaxKeyword:
                    ParseSyntaxRuleDeclaration(out node, errors);
                    break;

                case TokenKind.TokenKeyword:
                    ParseTokenRuleDeclaration(out node, errors);
                    break;

                default:
                    node = null;
                    errors.Add(_errorBuilder.MakeMissingToken(_stream.Current));
                    _stream.MoveNext();
                    break;
            }
        }

        private void ParseTokenRuleDeclaration(out RuleDeclarationSyntax node, ParserErrorCollection errors)
        {
            string name;
            var nodes = new List<RuleExpressionSyntax>();
            ConsumeToken(errors, TokenKind.TokenKeyword);
            ConsumeToken(errors, TokenKind.Identifier, out name);
            ConsumeToken(errors, TokenKind.EqualsToken);
            while (_stream.Current.Kind != TokenKind.SemicolonToken)
            {
                RuleExpressionSyntax child;
                ParseRuleExpression(out child, errors);
                if (child != null)
                {
                    nodes.Add(child);
                }
            }
            ConsumeToken(errors, TokenKind.SemicolonToken);
            node = new TokenRuleDeclarationSyntax(name, nodes);
        }

        private void ParseSyntaxRuleDeclaration(out RuleDeclarationSyntax node, ParserErrorCollection errors)
        {
            string name;
            var nodes = new List<RuleExpressionSyntax>();
            ConsumeToken(errors, TokenKind.SyntaxKeyword);
            ConsumeToken(errors, TokenKind.Identifier, out name);
            ConsumeToken(errors, TokenKind.EqualsToken);
            while (_stream.Current.Kind != TokenKind.SemicolonToken)
            {
                RuleExpressionSyntax child;
                ParseRuleExpression(out child, errors);
                if (child != null)
                {
                    nodes.Add(child);
                }
            }
            ConsumeToken(errors, TokenKind.SemicolonToken);
            node = new TokenRuleDeclarationSyntax(name, nodes);
        }

        private void ParseRuleExpression(out RuleExpressionSyntax node, ParserErrorCollection errors)
        {
            switch (_stream.Current.Kind)
            {
                case TokenKind.StringLiteralToken:
                    ParseStringLiteralRuleExression(out node, errors);
                    break;

                case TokenKind.Identifier:
                    ParseRuleReferenceExpression(out node, errors);
                    break;

                default:
                    node = null;
                    errors.Add(_errorBuilder.MakeMissingToken(_stream.Current));
                    _stream.MoveNext();
                    break;
            }
        }

        private void ParseStringLiteralRuleExression(out RuleExpressionSyntax node, ParserErrorCollection errors)
        {
            string value;
            ConsumeToken(errors, TokenKind.StringLiteralToken, out value);
            node = new StringLiteralRuleExressionSyntax(value);
        }

        private void ParseRuleReferenceExpression(out RuleExpressionSyntax node, ParserErrorCollection errors)
        {
            string name;
            ConsumeToken(errors, TokenKind.Identifier, out name);
            node = new RuleReferenceExpressionSyntax(name);
        }


        private void ConsumeToken(ParserErrorCollection errors, TokenKind tokenKind)
        {
            if (_stream.Current.Kind != tokenKind)
            {
                var error = _errorBuilder.MakeMissingToken(_stream.Current);
                errors.Add(error);
            }
            else
            {
                _stream.MoveNext();
            }
        }

        private void ConsumeToken(ParserErrorCollection errors, TokenKind tokenKind, out string value)
        {
            value = null;
            if (_stream.Current.Kind != tokenKind)
            {
                var error = _errorBuilder.MakeMissingToken(_stream.Current);
                errors.Add(error);
            }
            else
            {
                value = _stream.Current.Value;
                _stream.MoveNext();
            }
        }


        /// <summary>
        ///     Parses a language
        /// </summary>
        /// <returns>an instance of <see cref="LanguageDeclarationSyntax" /></returns>
        public LanguageDeclarationSyntax ParseLanguageDeclaration()
        {
            LanguageDeclarationSyntax node;
            var errors = new ParserErrorCollection();
            ParseLanguageDeclaration(out node, errors);

            if (errors.Count != 0)
            {
                throw new ParserException(errors);
            }
            return node;
        }

        /// <summary>
        ///     Parses a rule declaration
        /// </summary>
        /// <returns>an instance of <see cref="ParseRuleDeclaration(Grammatika.ParserErrorCollection,out Grammatika.Syntax.RuleDeclarationSyntax)" /></returns>
        public RuleDeclarationSyntax ParseRuleDeclaration()
        {
            RuleDeclarationSyntax node;
            var errors = new ParserErrorCollection();
            ParseRuleDeclaration(errors, out node);

            if (errors.Count != 0)
            {
                throw new ParserException(errors);
            }
            return node;
        }

        /// <summary>
        ///     Parses a rule expression
        /// </summary>
        /// <returns>an instance of <see cref="RuleExpressionSyntax" /></returns>
        public RuleExpressionSyntax ParseRuleExpression()
        {

            RuleExpressionSyntax node;
            var errors = new ParserErrorCollection();
            ParseRuleExpression(out node, errors);

            if (errors.Count != 0)
            {
                throw new ParserException(errors);
            }
            return node;
        }
    }
}
