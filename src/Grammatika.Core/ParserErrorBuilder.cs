﻿using System;

namespace Grammatika
{
    using System.Text;

    /// <summary>
    ///     Represents a parser error builder
    /// </summary>
    internal class ParserErrorBuilder
    {
        internal ParserError MakeMissingToken(Token current)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("({0}): ", current.Location);
            builder.Append("error: ");
            builder.AppendFormat("Invalid token '{0}'", current.Value);
            return new ParserError(builder.ToString());
        }
    }
}