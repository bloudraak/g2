﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika
{
    using System;
    using System.Collections.Generic;
    using Grammatika.Syntax;

    /// <summary>
    ///     Represents the parser results
    /// </summary>
    public class ParseResults
    {
        /// <summary>
        ///     Creates a new instance of <see cref="ParseResults" />
        /// </summary>
        /// <param name="syntaxSyntaxTree">The syntax tree</param>
        /// <param name="errors">An error collection</param>
        public ParseResults(SyntaxTree syntaxSyntaxTree, IEnumerable<ParserError> errors)
        {
            SyntaxTree = syntaxSyntaxTree;
            Errors = new ParserErrorCollection(errors);
        }

        /// <summary>
        ///     Creates a new instance of <see cref="ParseResults" />
        /// </summary>
        public ParseResults(SyntaxNode node) : this(new SyntaxTree(node), ParserErrorCollection.Empty)
        {
        }

        /// <summary>
        ///     Gets the errors
        /// </summary>
        public ParserErrorCollection Errors { get; private set; }

        /// <summary>
        ///     Gets the syntax tree
        /// </summary>
        public SyntaxTree SyntaxTree { get; private set; }

        /// <summary>
        /// Returns true if the results has errors
        /// </summary>
        public bool HasErrors
        {
            get { return Errors.Count > 0; }
        }
    }
}
