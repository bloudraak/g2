﻿//
// A language that understands one word 'language'
//

language Grammatika
{
	syntax Main = 'language' Identifier '{' RuleDeclaration* '}';
	syntax RuleDeclaration = 'a';
	token Identifier = 'Language';
}