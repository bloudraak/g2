﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika
{
    using System;
    using System.CodeDom.Compiler;
    using System.IO;
    using Grammatika.Syntax;

    internal static class Program
    {
        private static void Main(string[] args)
        {
            var results = SyntaxTree.Parse(args[0]);
            if (results.HasErrors)
            {
                Console.Error.WriteLine(results.Errors);
            }
            else
            {
                Visitor visitor = new Visitor(Console.Out);
                results.SyntaxTree.Accept(visitor);
            }

        }
    }

    internal sealed class Visitor : SyntaxTreeVisitor
    {
        private readonly IndentedTextWriter _writer;

        public Visitor(TextWriter textWriter)
        {
            _writer = new IndentedTextWriter(textWriter, "  ");
        }

        public override void OnStartLanguageDefinition(LanguageDeclarationSyntax node)
        {
            _writer.WriteLine("Language: {0}", node.Name);
            _writer.Indent++;
        }

        public override void OnEndLanguageDefinition(LanguageDeclarationSyntax node)
        {
            _writer.Indent--;
        }

        public override void OnStartTokenRuleDeclaration(TokenRuleDeclarationSyntax node)
        {
            _writer.WriteLine("Token: {0}", node.Name);
            _writer.Indent++;
        }

        public override void OnEndTokenRuleDeclaration(TokenRuleDeclarationSyntax node)
        {
            _writer.Indent--;
        }

        public override void OnStartSyntaxRuleDeclaration(SyntaxRuleDeclarationSyntax node)
        {
            _writer.WriteLine("Syntax: {0}", node.Name);
            _writer.Indent++;
        }

        public override void OnEndSyntaxRuleDeclaration(SyntaxRuleDeclarationSyntax node)
        {
            _writer.Indent--;
        }

        public override void OnStringLiteral(StringLiteralRuleExressionSyntax node)
        {
            _writer.WriteLine("String: '{0}'", node.Value);
        }

        public override void OnRuleReferenceExpression(RuleReferenceExpressionSyntax node)
        {
            _writer.WriteLine("${0}", node.Value);
        }
    }
}
