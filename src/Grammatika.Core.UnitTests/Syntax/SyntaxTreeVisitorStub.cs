﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika.Syntax
{
    using System;

    internal class SyntaxTreeVisitorStub : SyntaxTreeVisitor
    {
        public bool VisitedOnStartLanguageDefinition { get; set; }
        public bool VisitedOnEndLanguageDefinition { get; set; }
        public bool VisitedOnStartTokenRuleDeclaration { get; set; }
        public bool VisitedOnEndTokenRuleDeclaration { get; set; }
        public bool VisitedOnStringLiteral { get; set; }
        public bool VisitedOnRuleReferenceExpression { get; set; }
        public bool VisitedOnStartSyntaxRuleDeclaration { get; set; }
        public bool VisitedOnEndSyntaxRuleDeclaration { get; set; }

        public override void OnStartLanguageDefinition(LanguageDeclarationSyntax node)
        {
            VisitedOnStartLanguageDefinition = true;
        }

        public override void OnEndLanguageDefinition(LanguageDeclarationSyntax node)
        {
            VisitedOnEndLanguageDefinition = true;
        }

        public override void OnStartTokenRuleDeclaration(TokenRuleDeclarationSyntax node)
        {
            VisitedOnStartTokenRuleDeclaration = true;
        }

        public override void OnEndTokenRuleDeclaration(TokenRuleDeclarationSyntax node)
        {
            VisitedOnEndTokenRuleDeclaration = true;
        }

        public override void OnStartSyntaxRuleDeclaration(SyntaxRuleDeclarationSyntax node)
        {
            VisitedOnStartSyntaxRuleDeclaration = true;
        }

        public override void OnEndSyntaxRuleDeclaration(SyntaxRuleDeclarationSyntax node)
        {
            VisitedOnEndSyntaxRuleDeclaration = true;
        }

        public override void OnStringLiteral(StringLiteralRuleExressionSyntax node)
        {
            VisitedOnStringLiteral = true;
        }

        public override void OnRuleReferenceExpression(RuleReferenceExpressionSyntax node)
        {
            VisitedOnRuleReferenceExpression = true;
        }
    }
}
