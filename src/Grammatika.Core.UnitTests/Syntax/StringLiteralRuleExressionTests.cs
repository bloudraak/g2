﻿using System;

namespace Grammatika.Syntax
{
    using NUnit.Framework;

    [TestFixture]
    public class StringLiteralRuleExressionTests
    {
        [Test]
        public void Accept()
        {
            // Arrange
            var visitor = new SyntaxTreeVisitorStub();
            var target = new StringLiteralRuleExressionSyntax("Dummy");

            // Act
            target.Accept(visitor);

            // Assert
            Assert.IsTrue(visitor.VisitedOnStringLiteral);
        }
    }
}