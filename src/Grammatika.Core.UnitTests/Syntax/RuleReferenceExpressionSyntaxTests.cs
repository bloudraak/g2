﻿using System;

namespace Grammatika.Syntax
{
    using NUnit.Framework;

    [TestFixture]
    public class RuleReferenceExpressionSyntaxTests
    {
        [Test]
        public void Accept()
        {
            // Arrange
            var visitor = new SyntaxTreeVisitorStub();
            var target = new RuleReferenceExpressionSyntax("Dummy");

            // Act
            target.Accept(visitor);

            // Assert
            Assert.IsTrue(visitor.VisitedOnRuleReferenceExpression);
        }
    }
}