﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika.Syntax
{
    using System;
    using System.IO;
    using NUnit.Framework;

    [TestFixture]
    public class SyntaxTreeTests
    {
        private string path;

        [SetUp]
        public void SetUp()
        {
            path = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        }

        [TearDown]
        public void TearDown()
        {
            try
            {
                File.Delete(path);
            }
            catch (IOException e)
            {
                Console.Error.WriteLine(e);
            }
        }

        [Test]
        [TestCase("Language1")]
        [TestCase("Language2")]
        [TestCase("Language3")]
        [TestCase("Language4")]
        public void Parse(string resourceName)
        {
            // Arrange
            string source = Languages.ResourceManager.GetString(resourceName, Languages.Culture);
            File.WriteAllText(path, source);

            // Act
            var actual = SyntaxTree.Parse(path);

            // Assert
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.SyntaxTree);
            Assert.IsEmpty(actual.Errors);
        }
    }
}
