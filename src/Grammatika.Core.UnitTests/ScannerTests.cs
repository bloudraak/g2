﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika
{
    using System;
    using System.Linq;
    using NUnit.Framework;

    [TestFixture]
    public class ScannerTests
    {
        [Test]
        [TestCase("language", TokenKind.LanguageKeyword, "language", 1,1,1,9)]
        [TestCase("Language", TokenKind.Identifier, "Language",1,1,1,9)]
        [TestCase("'language'", TokenKind.StringLiteralToken, "language",1,1,1,11)]
        [TestCase("{", TokenKind.OpenBraceToken, "{",1,1,1,2)]
        [TestCase("}", TokenKind.CloseBraceToken, "}",1,1,1,2)]
        [TestCase("syntax", TokenKind.SyntaxKeyword, "syntax", 1, 1, 1, 7)]
        [TestCase("token", TokenKind.SyntaxKeyword, "token", 1, 1, 1, 6)]
        [TestCase("=", TokenKind.EqualsToken, "=",1,1,1,2)]
        [TestCase(";", TokenKind.SemicolonToken, ";",1,1,1,2)]
        public void Scan(string source, TokenKind kind, string value, int startLine, int startColumn, int endLine, int endColumn)
        {
            // Arrange
            var expectedLocation = new SourceLocation(startLine, startColumn, endLine, endColumn);
            var stream = new CharacterStream(source);
            var target = new Scanner(stream);

            // Act
            var tokens = target.Scan().ToArray();

            // Assert
            Assert.AreEqual(CharacterStream.InvalidCharacter, stream.Peek());
            Assert.IsNotEmpty(tokens);
            var token = tokens[0];
            Assert.IsNotNull(token);
            Assert.AreEqual(kind, token.Kind, "The kind mismatch");
            Assert.AreEqual(value, token.Value, "The value mismatch");
            Assert.AreEqual(expectedLocation, token.Location, "The locations mismatch");
        }

        [Test]
        [TestCase("// langauge")]
        [TestCase("// langauge\n")]
        [TestCase("// langauge\r\n")]
        public void Scan_WithComment(string source)
        {
            // Arrange
            CharacterStream stream = new CharacterStream(source);
            var target = new Scanner(stream);

            // Act
            var tokens = target.Scan().ToArray();

            // Assert
            Assert.AreEqual(CharacterStream.InvalidCharacter, stream.Peek());
            Assert.IsEmpty(tokens);
        }
    }
}
