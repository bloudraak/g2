﻿// The MIT License (MIT)
// 
// Copyright (c) 2014 Werner Strydom
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace Grammatika
{
    using System;
    using Grammatika.Syntax;
    using NUnit.Framework;

    [TestFixture]
    public class ParserTests
    {
        [Test]
        public void ParseLanguageDeclaration()
        {
            // Arrange
            Token[] tokens =
            {
                new Token(TokenKind.LanguageKeyword),
                new Token(TokenKind.Identifier, "Language"),
                new Token(TokenKind.OpenBraceToken),
                new Token(TokenKind.SyntaxKeyword),
                new Token(TokenKind.Identifier, "Main"),
                new Token(TokenKind.EqualsToken),
                new Token(TokenKind.StringLiteralToken, "language"),
                new Token(TokenKind.SemicolonToken),
                new Token(TokenKind.CloseBraceToken)
            };
            TokenStream stream = new TokenStream(tokens);
            var target = new Parser(stream);
            ParserException exception = null;
            LanguageDeclarationSyntax actual = null;
            
            // Act
            try
            {
                actual = target.ParseLanguageDeclaration();
            }
            catch (ParserException e)
            {
                exception = e;
            }

            // Assert
            Assert.IsNull(exception, "Errors: {0}", exception);
            Assert.IsNotNull(actual);
            Assert.AreEqual("Language", actual.Name);
            Assert.IsNotEmpty(actual.Rules);
        }

        [Test]
        public void ParseRuleDeclaration()
        {
            // Arrange
            Token[] tokens =
            {
                new Token(TokenKind.SyntaxKeyword),
                new Token(TokenKind.Identifier, "Main"),
                new Token(TokenKind.EqualsToken),
                new Token(TokenKind.StringLiteralToken, "language"),
                new Token(TokenKind.SemicolonToken)
            };
            TokenStream stream = new TokenStream(tokens);
            var target = new Parser(stream);

            // Act
            var actual = target.ParseRuleDeclaration();

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual("Main", actual.Name);
            Assert.IsNotEmpty(actual.Expressions);
        }

        [Test]
        public void ParseRuleExpression()
        {
            // Arrange
            Token[] tokens =
            {
                new Token(TokenKind.StringLiteralToken, "language"),
                new Token(TokenKind.SemicolonToken)
            };
            TokenStream stream = new TokenStream(tokens);
            var target = new Parser(stream);

            // Act
            var actual = target.ParseRuleExpression();

            // Assert
            Assert.IsNotNull(actual);
            Assert.IsInstanceOf<StringLiteralRuleExressionSyntax>(actual);
        }
    }
}
